let filter = require('../filter.cjs');

const items = [1, 2, 3, 4, 5, 5];
function cb(value, index, array) {
    return array[index] === 4;
}

let result = filter(items, cb);
console.log(result);