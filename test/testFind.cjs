let find = require('../find.cjs');

const items = [1, 2, 3, 4, 5, 5];
function cb(value) {
    return value >= 5.5;
}

let result = find(items, cb);
console.log(result);