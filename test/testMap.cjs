let map = require('../map.cjs');

const items = [1, 2, 3, 4, 5, 5];
function cb(element, index, array) {
    return element * array[index];
}

let result = map(items, cb);
console.log(result);

module.exports = cb;