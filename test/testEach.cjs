let each = require('../each.cjs');

const items = [1, 2, 3, 4, 5, 5];
function cb(element, index) {
    console.log("The Element is " + element + " and index is " + index);
}

let result = each(items, cb);
console.log(result);