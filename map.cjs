
function map(elements, cb) {
    let newArray = [];
    if (!Array.isArray(elements) || elements.length == 0) {
        return [];
    }
    for (let index = 0; index < elements.length; index++) {
        newArray.push(cb(elements[index], index, elements));
    }
    return newArray;
}

module.exports = map;