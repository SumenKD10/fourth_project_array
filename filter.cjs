function filter(elements, cb) {
    let answer = [];
    if (!Array.isArray(elements) || elements.length == 0) {
        return [];
    }
    for (let index = 0; index < elements.length; index++) {
        if (cb(elements[index], index, elements) === true) {
            answer.push(elements[index]);
        }
    }
    return answer;
}

module.exports = filter;