let answer = [];
function flatten(elements, depth = 1) {
    if (!Array.isArray(elements) || elements.length === 0) {
        return [];
    }
    for (let index = 0; index < elements.length; index++) {
        if (Array.isArray(elements[index])) {
            if (depth >= 1) {
                flatten(elements[index], depth - 1);
            } else {
                answer.push(elements[index]);
            }
        } else {
            if (elements[index] != undefined) {
                answer.push(elements[index]);
            }
        }
    }
    return answer;
}

module.exports = flatten;