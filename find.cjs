function find(elements, cb) {
    if (!Array.isArray(elements) || elements.length == 0) {
        return [];
    }
    for (let index = 0; index < elements.length; index++) {
        if (cb(elements[index])) {
            return elements[index];
        }
    }
    return undefined;
}

module.exports = find;