function reduce(elements, cb, startingValue) {
    let answer;
    if (!Array.isArray(elements) || elements.length == 0) {
        return [];
    }
    let index = 0;
    if (startingValue === undefined) {
        startingValue = elements[0];
        index = 1;
    }
    for (; index < elements.length; index++) {
        answer = cb(startingValue, elements[index], index, elements);
        startingValue = answer;
    }
    return answer;
}

module.exports = reduce;