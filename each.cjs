function each(elements, cb) {
    if (!Array.isArray(elements) || elements.length == 0) {
        return [];
    }
    for (let index = 0; index < elements.length; index++) {
        elements[index] = cb(elements[index], index);
    }
    return 'Operation Done';
}

module.exports = each;